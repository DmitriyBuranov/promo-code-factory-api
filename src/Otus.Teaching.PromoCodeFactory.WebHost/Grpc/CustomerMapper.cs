﻿using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc
{
    public static class CustomerMapper
    {
        public static Models.CreateOrEditCustomerRequest MapToModel(CreateOrEditCustomerRequest request)
        {
            return new Models.CreateOrEditCustomerRequest()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PreferenceIds = request.PreferenceIds?
                    .Select(x =>
                    {
                        Guid.TryParse(x, out var id);
                        return id;
                    })
                    .Where(id => id != Guid.Empty)
                    .ToList()
            };
        }
        
        public static CustomerShortResponse MapFromModel(Models.CustomerShortResponse model)
        {
            return new CustomerShortResponse()
            {
                Id = model.Id.ToString(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email
            };
        }
        
        public static CustomerResponse MapFromModel(Models.CustomerResponse model)
        {
            var response = new CustomerResponse()
            {
                Id = model.Id.ToString(),
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email
            };
            if (model.Preferences != null)
                response.Preferences.Add(model.Preferences.Select(MapFromModel));
            
            return response;
        }

        private static PreferenceResponse MapFromModel(Models.PreferenceResponse model)
        {
            return new PreferenceResponse()
            {
                Id = model.Id.ToString(),
                Name = model.Name,
            };
        }
    }
}
