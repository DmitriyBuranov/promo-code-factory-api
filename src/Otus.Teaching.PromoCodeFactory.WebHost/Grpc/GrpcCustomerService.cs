﻿using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.WebHost.Services.Customers;
using System;
using Google.Protobuf.WellKnownTypes;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc
{
    public class GrpcCustomerService: Customers.CustomersBase
    {
        private readonly ICustomersService customersService;

        public GrpcCustomerService(ICustomersService customersService)
        {
            this.customersService = customersService;
        }

        public override async Task<GetAllResponse> GetAll(Empty request, ServerCallContext context)
        {
            var customers = await customersService.GetAllAsync();
            var response = new GetAllResponse();
            response.List.AddRange(customers.Select(CustomerMapper.MapFromModel));
            return response;
        }

        public override async Task<CustomerResponse> GetById(IdRequest request, ServerCallContext context)
        {
            Guid id;
            if (!Guid.TryParse(request.Id, out id))
                throw new RpcException(Status.DefaultCancelled,
                    $"Wrong identifier '{request.Id}'.");

            var customer = await customersService.GetByIdAsync(id);
            if (customer is null)
                throw new RpcException(Status.DefaultCancelled,
                    $"Customer by identifier '{id}' not found.");

            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<CustomerResponse> Create(CreateCustomerRequest request, ServerCallContext context)
        {
            var customer = await customersService.CreateAsync(
                CustomerMapper.MapToModel(request.Payload));

            return CustomerMapper.MapFromModel(customer);
        }

        public override async Task<Empty> Edit(EditCustomerRequest request, ServerCallContext context)
        {
            Guid id;
            if (!Guid.TryParse(request.Id, out id))
                throw new RpcException(Status.DefaultCancelled,
                    $"Wrong identifier '{request.Id}'.");

            var result = await customersService.EditAsync(id, CustomerMapper.MapToModel(request.Payload));
            if (!result)
                throw new RpcException(Status.DefaultCancelled,
                    $"Customer by identifier '{id}' not found.");

            return new Empty();
        }

        public override async Task<Empty> Delete(IdRequest request, ServerCallContext context)
        {
            Guid id;
            if (!Guid.TryParse(request.Id, out id))
                throw new RpcException(Status.DefaultCancelled,
                    $"Wrong identifier '{request.Id}'.");

            var result = await customersService.DeleteAsync(id);
            if (!result)
                throw new RpcException(Status.DefaultCancelled,
                    $"Customer by identifier '{id}' not found.");

            return new Empty();
        }
    }
}
