﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Customers
{
    public class CustomersService : ICustomersService
    {

        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Preference> preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            this.customerRepository = customerRepository;
            this.preferenceRepository = preferenceRepository;
        }

        public async Task<CustomerResponse> CreateAsync(CreateOrEditCustomerRequest request)
        {
            //Получаем предпочтения из бд и сохраняем большой объект

            IEnumerable<Preference> preferences = new List<Preference>();
            if(request.PreferenceIds.Any())
            {
                preferences = await preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);
            }            

            Customer customer = CustomerMapper.MapFromModel(request, preferences);
            await customerRepository.AddAsync(customer);
            return new CustomerResponse(customer);
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return false;

            await customerRepository.DeleteAsync(customer);
            return true;
        }

        public async Task<bool> EditAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer == null)
                return false;

            IEnumerable<Preference> preferences = new List<Preference>();
            if (request.PreferenceIds.Any())
            {
                preferences = await preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            }

            CustomerMapper.MapFromModel(request, preferences, customer);
            await customerRepository.UpdateAsync(customer);

            return true;
        }

        public async Task<IEnumerable<CustomerShortResponse>> GetAllAsync()
        {
            var customers = await customerRepository.GetAllAsync();

            return customers
                .Select(x => new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                });
        }

        public async Task<CustomerResponse> GetByIdAsync(Guid id)
        {
            var customer = await customerRepository.GetByIdAsync(id);
            return customer != null ? new CustomerResponse(customer) : null;            
        }
    }
}
