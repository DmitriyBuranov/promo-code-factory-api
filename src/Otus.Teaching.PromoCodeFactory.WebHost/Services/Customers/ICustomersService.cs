﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services.Customers
{
    public interface ICustomersService
    {
        Task<IEnumerable<CustomerShortResponse>> GetAllAsync();
        Task<CustomerResponse> GetByIdAsync(Guid id);
        Task<CustomerResponse> CreateAsync(CreateOrEditCustomerRequest request);
        Task<bool> EditAsync(Guid id, CreateOrEditCustomerRequest request);
        Task<bool> DeleteAsync(Guid id);
    }
}
